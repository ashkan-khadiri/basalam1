package com.example.myapplication.service

import com.example.myapplication.view.BaSalamImageView
import com.facebook.drawee.view.SimpleDraweeView
import java.lang.IllegalStateException

class FrescoImageLoadingService : ImageLoadingService {
    override fun load(imageView: BaSalamImageView, imageUrl: String) {
        if(imageView is SimpleDraweeView) {
            imageView.setImageURI(imageUrl)
        }
        else
            throw IllegalStateException("ImageView must be instance of SimpleDraweeView")
    }
}