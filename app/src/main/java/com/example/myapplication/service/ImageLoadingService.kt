package com.example.myapplication.service

import com.example.myapplication.view.BaSalamImageView

interface ImageLoadingService {
    fun load(image : BaSalamImageView, imageUrl : String)
}