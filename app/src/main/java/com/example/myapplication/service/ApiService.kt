package com.example.myapplication.service

import com.example.myapplication.data.Animal
import com.example.myapplication.data.Flower
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {

    @GET("intern.android/")
    fun getFlower(@Query("kind") kind : String) : Single<List<Flower>>

    @GET("intern.android")
    fun getAnimal(@Query("kind") kind : String) : Single<List<Animal>>

}

fun createApiService() : ApiService {
    val retrofit = Retrofit.Builder()
        .baseUrl("https://kashkool.basalam.com/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    return retrofit.create(ApiService::class.java)
}