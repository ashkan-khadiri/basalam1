package com.example.myapplication.data.repo

import com.example.myapplication.data.Flower
import io.reactivex.Single

interface FlowerRepository {

    fun getFlower(kind: String) : Single<Flower>
}