package com.example.myapplication.data.repo

import com.example.myapplication.data.Animal
import com.example.myapplication.data.Flower
import io.reactivex.Single

interface AnimalRepository {

    fun getAnimal(kind: String) :  Single<Animal>

}