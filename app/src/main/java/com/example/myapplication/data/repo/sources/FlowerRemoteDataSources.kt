package com.example.myapplication.data.repo.sources

import com.example.myapplication.data.Flower
import com.example.myapplication.service.https.ApiService
import io.reactivex.Single

class FlowerRemoteDataSources(val apiService: ApiService) : FlowerDataSources {

    override fun getFlower(kind: String): Single<Flower> {
        return apiService.getFlower(kind)
    }


}