package com.example.myapplication.data.repo.sources

import com.example.myapplication.data.Flower
import io.reactivex.Single

interface FlowerDataSources {

    fun getFlower(kind: String): Single<Flower>

}