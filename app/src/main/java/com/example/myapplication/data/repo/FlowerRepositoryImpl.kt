package com.example.myapplication.data.repo

import com.example.myapplication.data.Flower
import com.example.myapplication.data.repo.sources.FlowerDataSources
import io.reactivex.Single

class FlowerRepositoryImpl(val flowerRemoteDataSources: FlowerDataSources) : FlowerRepository {


    override fun getFlower(kind: String) : Single<Flower> {
       return flowerRemoteDataSources.getFlower(kind)
    }
}