package com.example.myapplication.data.repo.sources

import com.example.myapplication.data.Animal
import com.example.myapplication.data.Flower
import com.example.myapplication.service.https.ApiService
import io.reactivex.Single

class AnimalRemoteDataSources(val apiService: ApiService) : AnimalDataSources {

    override fun getAnimal(kind: String):  Single<Animal> {
        return apiService.getAnimal(kind)
    }


}