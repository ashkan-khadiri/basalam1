package com.example.myapplication.data.repo.sources

import com.example.myapplication.data.Animal
import com.example.myapplication.data.Flower
import io.reactivex.Single

interface AnimalDataSources {

    fun getAnimal(kind : String) :  Single<Animal>

}