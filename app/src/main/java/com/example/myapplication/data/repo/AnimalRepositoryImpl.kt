package com.example.myapplication.data.repo

import com.example.myapplication.data.Animal
import com.example.myapplication.data.repo.sources.AnimalDataSources
import io.reactivex.Single

class AnimalRepositoryImpl(val animalRemoteDataSources: AnimalDataSources) : AnimalRepository {


    override fun getAnimal(kind: String) : Single<Animal>  {
       return animalRemoteDataSources.getAnimal(kind)
    }
}