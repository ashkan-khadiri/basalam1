package com.example.myapplication.data

import android.os.Parcelable
import kotlinx.parcelize.Parceler
import kotlinx.parcelize.Parcelize

@kotlinx.android.parcel.Parcelize
data class Data(
    val id: Int,
    val image: String,
    val name: String
) : Parcelable