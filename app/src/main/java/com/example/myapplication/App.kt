package com.example.myapplication

import android.app.Application
import com.example.myapplication.adapter.RecyclerAdapter
import com.example.myapplication.data.Data
import com.example.myapplication.data.repo.AnimalRepository
import com.example.myapplication.data.repo.AnimalRepositoryImpl
import com.example.myapplication.data.repo.FlowerRepository
import com.example.myapplication.data.repo.FlowerRepositoryImpl
import com.example.myapplication.data.repo.sources.AnimalRemoteDataSources
import com.example.myapplication.data.repo.sources.FlowerRemoteDataSources
import com.example.myapplication.featuer.MainActivityViewModel
import com.example.myapplication.service.FrescoImageLoadingService
import com.example.myapplication.service.ImageLoadingService
import com.example.myapplication.service.https.createApiService
import com.facebook.drawee.backends.pipeline.Fresco
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module
import timber.log.Timber

class App : Application() {

    override fun onCreate() {
        super.onCreate()

      Timber.plant(Timber.DebugTree())
      Fresco.initialize(this)

      val myModules = module {
          single { createApiService() }
          single<ImageLoadingService> { FrescoImageLoadingService() }
          factory<FlowerRepository> { FlowerRepositoryImpl(FlowerRemoteDataSources(get())) }
          factory<AnimalRepository> { AnimalRepositoryImpl(AnimalRemoteDataSources(get())) }
          viewModel { MainActivityViewModel(get(),get()) }
      }

        startKoin {
            this@App
            modules(myModules)
        }
    }

}