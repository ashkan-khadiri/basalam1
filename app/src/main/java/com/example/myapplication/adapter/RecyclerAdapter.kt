package com.example.myapplication.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.data.Data
import com.example.myapplication.service.ImageLoadingService
import com.example.myapplication.view.BaSalamImageView
import com.facebook.drawee.view.SimpleDraweeView
import com.google.android.material.floatingactionbutton.FloatingActionButton

class RecyclerAdapter(
    val imageLoadingService: ImageLoadingService
) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    var flowers = ArrayList<Data>()
    set(value) {
        field = value
        notifyDataSetChanged()
    }


    var onItemClickListener : OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.rv_item, parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBand(flowers[position])
    }

    override fun getItemCount(): Int {
        return flowers.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgFlower = itemView.findViewById<BaSalamImageView>(R.id.imgFlower)
        val imgAnimal = itemView.findViewById<BaSalamImageView>(R.id.imgAnimal)
        val txtTitle = itemView.findViewById<TextView>(R.id.txtTitle)
        val countLaterCommon = itemView.findViewById<TextView>(R.id.countCommon)
        val fabDetial = itemView.findViewById<FloatingActionButton>(R.id.fabDetail)

        fun onBand(flowers: Data) {
            imageLoadingService.load(imgFlower,flowers.image)
            txtTitle.setText(flowers.name)
            countLaterCommon.setText("${flowers.id}")

            fabDetial.setOnClickListener {
                onItemClickListener?.onClick(flowers)
            }
        }

    }

    interface OnItemClickListener{
        fun onClick(data : Data)
    }
}