package com.example.myapplication.featuer

import androidx.lifecycle.*
import com.example.myapplication.common.BaSalamSingleObserver
import com.example.myapplication.common.EXTRA_KEY_ANIMAL
import com.example.myapplication.common.EXTRA_KEY_FLOWER
import com.example.myapplication.data.Animal
import com.example.myapplication.data.Data
import com.example.myapplication.data.Flower
import com.example.myapplication.data.repo.AnimalRepository
import com.example.myapplication.data.repo.FlowerRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MainActivityViewModel(
    val flowerRepositoryImpl: FlowerRepository,
    val animalRepositoryImpl: AnimalRepository
) : ViewModel() {
    val disposable = CompositeDisposable()

    val flowers = MutableLiveData<List<Data>>()
    val animals = MutableLiveData<List<Data>>()

    init {
        flowerRepositoryImpl.getFlower(EXTRA_KEY_FLOWER)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : BaSalamSingleObserver<Flower>(disposable) {
                override fun onSuccess(t: Flower) {
                    flowers.value = t.data
                }
            })

        animalRepositoryImpl.getAnimal(EXTRA_KEY_ANIMAL)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(object : BaSalamSingleObserver<Animal>(disposable) {
                override fun onSuccess(t: Animal) {
                    animals.value = t.data
                }
            })

    }
}
