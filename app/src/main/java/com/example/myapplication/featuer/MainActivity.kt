package com.example.myapplication.featuer

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R
import com.example.myapplication.adapter.RecyclerAdapter
import com.example.myapplication.common.EXTRA_KEY_DATA
import com.example.myapplication.data.Data
import com.example.myapplication.ditail.DitailActivity
import com.example.myapplication.service.ImageLoadingService
import org.koin.android.ext.android.inject
import org.koin.android.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity(), RecyclerAdapter.OnItemClickListener {

    val mainActivityViewModel: MainActivityViewModel by viewModel()
    var recyclerAdapter: RecyclerAdapter? = null
    val imageLoadingService: ImageLoadingService by inject()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val rv = findViewById<RecyclerView>(R.id.rvMain)

        recyclerAdapter?.onItemClickListener = this

        rv.layoutManager =
            LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        mainActivityViewModel.flowers.observe(this) {
            recyclerAdapter = RecyclerAdapter(
                imageLoadingService)
            recyclerAdapter!!.flowers = it as ArrayList<Data>
            rv.adapter = recyclerAdapter
        }


    }

    override fun onClick(data: Data) {
        startActivity(Intent(this, DitailActivity::class.java).apply {
            putExtra(EXTRA_KEY_DATA, data)
        })
    }
}