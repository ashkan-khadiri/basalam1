package com.example.myapplication.common

const val EXTRA_KEY_FLOWER = "flower"
const val EXTRA_KEY_ANIMAL = "animal"

const val EXTRA_KEY_DATA = "data"